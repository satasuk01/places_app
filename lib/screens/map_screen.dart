import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:places_app/models/place.dart';

class MapScreen extends StatefulWidget {
  // MapScreen({Key? key}) : super(key: key);
  final PlaceLocation initLocation;
  final bool isSelecting;

  MapScreen({
    this.initLocation =
        const PlaceLocation(latitude: 37.422, longitude: -122.084),
    this.isSelecting = false,
  });

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  LatLng? _pickedLocation;

  void _selectLocation(LatLng position) {
    setState(() {
      _pickedLocation = position;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Your Map"),
        actions: [
          if (widget.isSelecting)
            IconButton(
              onPressed: () {
                if (_pickedLocation == null) {
                } else {}
                Navigator.of(context).pop(_pickedLocation);
              },
              icon: Icon(Icons.check),
            ),
        ],
      ),
      body: GoogleMap(
        initialCameraPosition: CameraPosition(
          zoom: 16,
          target: LatLng(
            widget.initLocation.latitude,
            widget.initLocation.longitude,
          ),
        ),
        onTap: widget.isSelecting ? _selectLocation : null,
        markers: (_pickedLocation == null && widget.isSelecting)
            ? {}
            : {
                Marker(
                  markerId: MarkerId('m1'),
                  position: _pickedLocation ??
                      LatLng(
                        widget.initLocation.latitude,
                        widget.initLocation.longitude,
                      ),
                ),
              },
      ),
    );
  }
}
